//
//  School.h
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/16/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <UIKit/UIKit.h>

@interface School : JSONModel

@property(nonatomic) NSString *dbn;
@property(nonatomic) NSString *schoolName;
@property(nonatomic) NSString *website;
@property(nonatomic) NSString *phoneNumber;
@property(nonatomic) NSString *latitude;
@property(nonatomic) NSString *longitude;
//@property(nonatomic) NSNumber <Ignore> *index;

@end
