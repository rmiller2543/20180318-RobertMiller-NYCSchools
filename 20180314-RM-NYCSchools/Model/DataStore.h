//
//  DataStore.h
//  20180314-RM-NYCSchools
//
//  Created by Robert Miller on 3/18/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "School.h"
#import "SchoolScores.h"

@interface DataStore : NSObject

+ (instancetype _Nonnull)sharedInstance;

- (void)getSchoolsWithCompletion:(void(^_Nonnull)(NSArray * _Nullable schools))completion;
- (void)getSchoolImageURLForSchool:(School *_Nonnull)school withCompletion:(void(^_Nonnull)(NSString * _Nullable urlString))completion;
- (void)getSchoolScoresForSchool:(School *_Nonnull)school withCompletion:(void(^_Nonnull)(SchoolScores * _Nullable scores))completion;

@end
