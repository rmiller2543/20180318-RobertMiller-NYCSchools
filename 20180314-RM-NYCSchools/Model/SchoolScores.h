//
//  SchoolScores.h
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/16/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface SchoolScores : JSONModel

@property(nonatomic) NSString *dbn;
@property(nonatomic) NSString *numOfSatTestTakers;
@property(nonatomic) NSString *satCriticalReadingAvgScore;
@property(nonatomic) NSString *satMathAvgScore;
@property(nonatomic) NSString *satWritingAvgScore;

@end
