//
//  School.m
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/16/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import "School.h"

@interface School()

@end

@implementation School

+ (JSONKeyMapper *)keyMapper {
    return [JSONKeyMapper mapperForSnakeCase];
}

@end
