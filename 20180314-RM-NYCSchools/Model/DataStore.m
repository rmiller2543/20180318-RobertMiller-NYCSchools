//
//  DataStore.m
//  20180314-RM-NYCSchools
//
//  Created by Robert Miller on 3/18/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import "DataStore.h"
#import "NSString+PlacesURL.h"
#import <RMDownloadAdapter/RMDownloadAdapter.h>

@interface DataStore()

@property(nonatomic,strong) NSArray *schools;
@property(nonatomic,strong) NSMutableDictionary *schoolScores;

@end

#define PLACES_API_KEY @"AIzaSyCuG0Z6QpUA2MuQj9TDLzUo5aYqyg70dww"

@implementation DataStore

+ (instancetype)sharedInstance {
    
    static DataStore *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataStore alloc] init];
    });
    
    return sharedInstance;
    
}

- (void)getSchoolsWithCompletion:(void (^)(NSArray * _Nullable))completion {
    
    if (self.schools != nil) {
        completion(self.schools);
        return;
    }
    NSString *urlString = @"https://data.cityofnewyork.us/resource/97mf-9njv.json";
    [self getSchoolsWithEndPoint:urlString andCallback:^(NSData *data) {
        NSError *err = nil;
        NSArray *cityData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        NSMutableArray *tempDataSource = [[NSMutableArray alloc] init];
        for (NSDictionary *schoolDict in cityData) {
            NSError *jsonError = nil;
            School *school = [[School alloc] initWithDictionary:schoolDict error:&jsonError];
            if (school != nil) {
                [tempDataSource addObject:school];
            }
        }
        self.schools = [NSArray arrayWithArray:tempDataSource];
        completion(self.schools);
    }];
    
}

- (void)getSchoolImageURLForSchool:(School *)school withCompletion:(void (^)(NSString * _Nullable))completion {
    
    [self downloadPlaceForSchool:school withCallback:^(id object) {
        
        NSDictionary *place = (NSDictionary *)object;
        
        NSArray *photos = [place objectForKey:@"photos"];
        NSDictionary *photo = [photos firstObject];
        NSString *reference = [photo objectForKey:@"photo_reference"];
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=1400&photoreference=%@&key=%@", reference, PLACES_API_KEY];
        
        completion(urlString);
        
    }];
    
}

- (void)getSchoolScoresForSchool:(School *)school withCompletion:(void (^)(SchoolScores * _Nullable))completion {
    
    if (self.schoolScores != nil && [self.schoolScores objectForKey:school.dbn] != nil) {
        completion([self.schoolScores objectForKey:school.dbn]);
        return;
    }
    
    NSString *urlString = @"https://data.cityofnewyork.us/resource/734v-jeq5.json";
    [self getSchoolScoresWithEndPoint:urlString andCallback:^(NSData *data) {
        
        NSError *err = nil;
        NSArray *scoreData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
        
        SchoolScores *scores = [[SchoolScores alloc] initWithDictionary:[scoreData firstObject] error:&err];
        
        if (self.schoolScores == nil) {
            self.schoolScores = [[NSMutableDictionary alloc] init];
        }
        
        [self.schoolScores setObject:scores forKey:school.dbn];
        
        completion(scores);
        
    }];
    
}


- (void)downloadPlaceForSchool:(School *)school withCallback:(void(^)(id object))callback {
    
    NSString *urlString = [[NSString alloc] getPlacesUrlFromSchool:school];
    NSIndexPath *indexPath = [NSIndexPath indexPathWithIndex:[self.schools indexOfObject:school]];
    [[RMDownloadAdapter sharedInstance] startDownloadWithURLString:urlString withClass:[NSDictionary class] indexPath:indexPath progressBlock:nil completionBlock:^(id object, NSError *error) {
        
        if (error == nil) {
            
            NSArray *places = (NSArray *)object;
            if (places > 0) {
                callback(places.firstObject);
            } else {
                callback(nil);
            }
            
        } else if (error.code == RMDownloadAdapterErrorCodeInvalidClass) {
            
            NSError *err = nil;
            NSDictionary *results = [NSJSONSerialization JSONObjectWithData:object options:NSJSONReadingMutableLeaves error:&err];
            
            NSArray *places = [results objectForKey:@"results"];
            if (places > 0) {
                callback(places.firstObject);
            } else {
                callback(nil);
            }
            
        }
        
    }];
    
}


-(void)getSchoolsWithEndPoint:(NSString *)endPoint andCallback:(void(^)(NSData *data))callback {
    
    NSURL *url = [NSURL URLWithString:endPoint];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    [request setValue:@"fgu5sprviAa511QWfjFcDTjiS" forHTTPHeaderField:@"X-App-Token"];
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        //up up
        callback(data);
    }];
    
    [dataTask resume];
    
}

-(void)getSchoolScoresWithEndPoint:(NSString *)endPoint andCallback:(void(^)(NSData *data))callback {
    
    NSURL *url = [NSURL URLWithString:endPoint];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    [request setValue:@"fgu5sprviAa511QWfjFcDTjiS" forHTTPHeaderField:@"X-App-Token"];
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        //up up
        callback(data);
    }];
    
    [dataTask resume];
    
}

@end
