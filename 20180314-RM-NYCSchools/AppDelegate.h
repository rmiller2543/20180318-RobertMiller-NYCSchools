//
//  AppDelegate.h
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/14/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

