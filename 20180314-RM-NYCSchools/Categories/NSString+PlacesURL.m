//
//  NSString+PlacesURL.m
//  20180314-RM-NYCSchools
//
//  Created by Robert Miller on 3/18/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import "NSString+PlacesURL.h"

@implementation NSString (PlacesURL)

- (NSString *)getPlacesUrlFromSchool:(School *)school {
    
    NSString *googlePlaceUrlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%@,%@&radius=50&type=%@&name=%@&key=%@", school.latitude, school.longitude, @"school", [self sanitizedName:school.schoolName], @"AIzaSyCuG0Z6QpUA2MuQj9TDLzUo5aYqyg70dww"];
    
    NSLog(@"url for google place: %@", googlePlaceUrlString);
    
    return googlePlaceUrlString;
    
}

//Sanitizing the name so we can add it as a query item in the url
- (NSString *)sanitizedName:(NSString *)itemName {
    
    itemName = [itemName stringByReplacingOccurrencesOfString:@"'" withString:@""];
    itemName = [itemName stringByReplacingOccurrencesOfString:@"&" withString:@""];
    itemName = [itemName stringByReplacingOccurrencesOfString:@"(" withString:@""];
    itemName = [itemName stringByReplacingOccurrencesOfString:@")" withString:@""];
    itemName = [itemName stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    return [itemName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
}

@end
