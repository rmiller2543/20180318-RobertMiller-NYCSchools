//
//  NSString+PlacesURL.h
//  20180314-RM-NYCSchools
//
//  Created by Robert Miller on 3/18/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "School.h"

@interface NSString (PlacesURL)

- (NSString *)getPlacesUrlFromSchool:(School *)school;

@end
