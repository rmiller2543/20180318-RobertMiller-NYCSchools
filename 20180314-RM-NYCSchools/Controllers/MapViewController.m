//
//  MapViewController.m
//  20180314-RM-NYCSchools
//
//  Created by Robert Miller on 3/15/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import "MapViewController.h"
#import "TableViewController.h"
#import "DetailsViewController.h"
#import "School.h"
#import <LTInfiniteScrollView/LTInfiniteScrollView.h>
#import "CardView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <RMDownloadAdapter/RMDownloadAdapter.h>
#import "NSString+PlacesURL.h"
#import "DataStore.h"
@import GoogleMaps;

@interface MapViewController () <LTInfiniteScrollViewDelegate, LTInfiniteScrollViewDataSource, GMSMapViewDelegate, CardViewDelegate>

@property(nonatomic,weak) IBOutlet GMSMapView *mapView;
@property(nonatomic,weak) IBOutlet LTInfiniteScrollView *scrollView;

@property(nonatomic,strong) NSArray <School*> *dataSource;
@property(nonatomic,strong) NSMutableArray <GMSMarker*> *markers;
@property(nonatomic,strong) GMSMarker *prevMarker;

@end

typedef void(^RequestDataCallback)(NSData *data);

@implementation MapViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *rightNavButton = [[UIBarButtonItem alloc] initWithTitle:@"List" style:UIBarButtonItemStylePlain target:self action:@selector(showListView:)];
    self.navigationItem.rightBarButtonItem = rightNavButton;
    self.navigationItem.title = @"Map of Schools";
    
    self.markers = [[NSMutableArray alloc] init];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:40.7128 longitude:-74.0060 zoom:12];
    [self.mapView setCamera:camera];
    
    [[DataStore sharedInstance] getSchoolsWithCompletion:^(NSArray * _Nullable schools) {
        
        self.dataSource = schools;
        [self getImageURLsForSchools];
        [self reloadMap];
        [self.scrollView reloadDataWithInitialIndex:0];
        
    }];
    
    self.scrollView.delegate = self;
    self.scrollView.dataSource = self;
    self.scrollView.scrollEnabled = YES;
    self.scrollView.pagingEnabled = YES;
    
    self.mapView.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadMap {
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    for (School *school in self.dataSource) {
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(school.latitude.floatValue, school.longitude.floatValue);
        GMSMarker *marker = [GMSMarker markerWithPosition:coordinate];
        marker.userData = school;
        marker.title = school.schoolName;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = self.mapView;
        [marker setIcon:[UIImage imageNamed:@"school-college-university-graduation-pin-location-marker-512"]];
        [self.markers addObject:marker];
        
        [bounds includingCoordinate:coordinate];
        
    }
    
    GMSCameraUpdate *camera = [GMSCameraUpdate fitBounds:bounds withPadding:30.0f];
    [self.mapView animateWithCameraUpdate:camera];
    
}

- (void)getImageURLsForSchools {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        for (int i = 0; i < 50; i++) {
        //for (School *school in self.dataSource) {
            School *school = [self.dataSource objectAtIndex:i];
            [[DataStore sharedInstance] getSchoolImageURLForSchool:school withCompletion:^(NSString * _Nullable urlString) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:school.dbn object:urlString userInfo:nil];
                
            }];
            
        }
        
    });
    
}

- (void)showListView:(id)sender {
    
    TableViewController *tableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"table"];
    
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:tableViewController animated:NO];
    
}


#pragma mark - Google Maps View Delegate Methods
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    
    School *school = (School*)marker.userData;
    NSInteger index = [self.dataSource indexOfObject:school];
    [self.scrollView scrollToIndex:index animated:YES];
    
    [self.prevMarker setIcon:[UIImage imageNamed:@"school-college-university-graduation-pin-location-marker-512"]];
    [marker setIcon:[UIImage imageNamed:@"school-college-university-graduation-pin-location-marker-512-selected"]];
    self.prevMarker = marker;
    
    [self.mapView setSelectedMarker:marker];
    
    return YES;
    
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    
    NSInteger index = [self.markers indexOfObject:marker];
    
    DetailsViewController *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"details"];
    detailsViewController.school = (School *)marker.userData;
    detailsViewController.index = index;
    
    [self.navigationController pushViewController:detailsViewController animated:YES];
    
}

#pragma mark - LTInfiniteScrollView Delegate & Data Source Methods
- (UIView *)viewAtIndex:(NSInteger)index reusingView:(UIView *)view {
    
    School *school = [self.dataSource objectAtIndex:index];
    if (view) {
        
        CardView *cardView = (CardView *)view;
        cardView.nameLabel.text = school.schoolName;
        cardView.index = [self.dataSource indexOfObject:school];
        
        [[DataStore sharedInstance] getSchoolImageURLForSchool:school withCompletion:^(NSString * _Nullable urlString) {
            [cardView.imageView sd_setImageWithURL:[NSURL URLWithString:urlString]];
        }];
        
        return cardView;
        
    }
    
    CardView *cardView = [[[NSBundle mainBundle] loadNibNamed:@"CardView" owner:self options:nil] objectAtIndex:0];
    CGRect frame = CGRectMake(0, 0, 1 * self.scrollView.frame.size.width / 3, 1 * self.scrollView.frame.size.height / 3);
    [cardView setFrame:frame];
    cardView.nameLabel.text = school.schoolName;
    cardView.index = [self.dataSource indexOfObject:school];
    [cardView setupDelegate:self];
    
    [[DataStore sharedInstance] getSchoolImageURLForSchool:school withCompletion:^(NSString * _Nullable urlString) {
        [cardView.imageView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    }];
    
    return cardView;
    
}

- (void)scrollView:(LTInfiniteScrollView *)scrollView didScrollToIndex:(NSInteger)index {
    
    GMSMarker *marker = [self.markers objectAtIndex:index];
    [marker setIcon:[UIImage imageNamed:@"school-college-university-graduation-pin-location-marker-512-selected"]];
    [self.mapView setSelectedMarker:marker];
    
    if (self.prevMarker != nil) {
        
        [self.prevMarker setIcon:[UIImage imageNamed:@"school-college-university-graduation-pin-location-marker-512"]];
        self.prevMarker = marker;
        
    }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:marker.position.latitude longitude:marker.position.longitude zoom:12];
    [self.mapView setCamera:camera];
    
}

- (void)updateView:(UIView *)view withProgress:(CGFloat)progress scrollDirection:(ScrollDirection)direction {
    
    if (progress == 0) {
        [self animateMenuOut:view];
    } else if(fabs(progress) == 1) {
        [self animateMenuBack:view];
    }
    
}

- (void)animateMenuOut:(UIView *)view {
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.56 initialSpringVelocity:0 options:0 animations:^{
        view.transform = CGAffineTransformMakeTranslation(0, -60);
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void)animateMenuBack:(UIView *)view {
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.56 initialSpringVelocity:0 options:0 animations:^{
        view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
    
}

- (NSInteger)numberOfViews {
    return self.dataSource.count;
}

- (NSInteger)numberOfVisibleViews {
    return 3;
}


#pragma mark - Card View Delegate
- (void)cardTapped:(UIView *)card {
    
    CardView *cardView = (CardView *)card;
    NSLog(@"card tapped at index: %li", (long)cardView.index);
    
    DetailsViewController *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"details"];
    detailsViewController.school = [self.dataSource objectAtIndex:cardView.index];
    detailsViewController.index = cardView.index;
    
    [self.navigationController pushViewController:detailsViewController animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
