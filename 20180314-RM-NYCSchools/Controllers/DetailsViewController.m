//
//  DetailsViewController.m
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/16/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import "DetailsViewController.h"
#import "SchoolScores.h"
#import <RMDownloadAdapter/RMDownloadAdapter.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+PlacesURL.h"
#import "DataStore.h"
@import GoogleMaps;

@interface DetailsViewController ()

@property(nonatomic,weak) IBOutlet UIImageView *imageView;
@property(nonatomic,weak) IBOutlet UILabel *schoolNameLabel;
@property(nonatomic,weak) IBOutlet UILabel *schoolPhoneLabel;
@property(nonatomic,weak) IBOutlet UILabel *schoolWebsiteLabel;
@property(nonatomic,weak) IBOutlet GMSMapView *mapView;
@property(nonatomic,weak) IBOutlet UILabel *numStudentsLabel;
@property(nonatomic,weak) IBOutlet UILabel *avgReadingLabel;
@property(nonatomic,weak) IBOutlet UILabel *avgMathLabel;
@property(nonatomic,weak) IBOutlet UILabel *avgWritingLabel;

@end

typedef void(^RequestDataCallback)(NSData *data);
#define API_KEY @"AIzaSyCuG0Z6QpUA2MuQj9TDLzUo5aYqyg70dww"

@implementation DetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [[DataStore sharedInstance] getSchoolScoresForSchool:self.school withCompletion:^(SchoolScores * _Nullable scores) {
        if (scores) {
            
            self.numStudentsLabel.text = scores.numOfSatTestTakers;
            self.avgMathLabel.text = scores.satMathAvgScore;
            self.avgReadingLabel.text = scores.satCriticalReadingAvgScore;
            self.avgWritingLabel.text = scores.satWritingAvgScore;
            
        }
    }];
    
    self.schoolNameLabel.text = self.school.schoolName;
    self.schoolPhoneLabel.text = self.school.phoneNumber;
    self.schoolWebsiteLabel.text = self.school.website;
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.school.latitude.floatValue, self.school.longitude.floatValue);
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:coordinate zoom:15];
    
    GMSMarker *marker = [GMSMarker markerWithPosition:coordinate];
    [marker setIcon:[UIImage imageNamed:@"school-college-university-graduation-pin-location-marker-512-selected"]];
    marker.map = self.mapView;
    
    [self.mapView setCamera:camera];
    
    [[DataStore sharedInstance] getSchoolImageURLForSchool:self.school withCompletion:^(NSString * _Nullable urlString) {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
