//
//  TableViewController.m
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/16/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import "TableViewController.h"
#import "School.h"
#import "DetailsViewController.h"
#import "DataStore.h"

@interface TableViewController () <UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate>

@property(nonatomic,strong) NSArray *dataSource;
@property(nonatomic,strong) NSArray *searchResults;
@property(nonatomic,strong) UISearchController *searchController;
@property(nonatomic,strong) UITableViewController *searchResultsController;

@property(nonatomic) BOOL searching;

@end

@implementation TableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UIBarButtonItem *rightNavButton = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStylePlain target:self action:@selector(showMapView:)];
    self.navigationItem.rightBarButtonItem = rightNavButton;
    self.navigationItem.title = @"List of Schools";
    
    [self.navigationItem setHidesBackButton:YES];
    
    [[DataStore sharedInstance] getSchoolsWithCompletion:^(NSArray * _Nullable schools) {
        self.dataSource = schools;
        [self.tableView reloadData];
    }];
    
    self.searchResultsController = [[UITableViewController alloc] init];
    self.searchResultsController.tableView.delegate = self;
    self.searchResultsController.tableView.dataSource = self;
    [self.searchResultsController.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.placeholder = nil;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = YES; // default is YES
    self.searchController.searchBar.delegate = self;
    
    self.definesPresentationContext = YES;
    
    self.searching = NO;
    
}

- (void)showMapView:(id)sender {
    
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searching) {
        return self.searchResults.count;
    }
    return self.dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    School *school = nil;
    if (self.searching) {
        if (indexPath.row > self.searchResults.count) {
            return cell;
        }
        school = [self.searchResults objectAtIndex:indexPath.row];
        
    } else {
        if (indexPath.row > self.dataSource.count) {
            return cell;
        }
        school = [self.dataSource objectAtIndex:indexPath.row];
    }
    
    // Configure the cell...
    cell.textLabel.text = school.schoolName;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DetailsViewController *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"details"];
    detailsViewController.school = [self.dataSource objectAtIndex:indexPath.row];
    detailsViewController.index = indexPath.row;
    
    [self.navigationController pushViewController:detailsViewController animated:YES];
    
}


#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searching = NO;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    self.searching = YES;
    
    NSString *searchTerm = searchController.searchBar.text;
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"SELF.schoolName contains[c] %@", searchTerm];
    
    self.searchResults = [self.dataSource filteredArrayUsingPredicate:searchPredicate];
    
    [self.searchResultsController.tableView reloadData];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
