//
//  DetailsViewController.h
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/16/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "School.h"

@interface DetailsViewController : UIViewController

@property(nonatomic,strong) School *school;
@property(nonatomic,assign) NSInteger index;

@end
