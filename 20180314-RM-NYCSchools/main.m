//
//  main.m
//  20180314-RM-NYCSchools
//
//  Created by RMiller on 3/14/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
