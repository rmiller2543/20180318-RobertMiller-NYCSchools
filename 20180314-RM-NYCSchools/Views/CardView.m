//
//  CardView.m
//  20180314-RM-NYCSchools
//
//  Created by Robert Miller on 3/18/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import "CardView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CardView()

@property(nonatomic,copy) NSString *dbn;

@end

@implementation CardView

- (void)listenForDbn:(NSString *)dbn {
    
    if (self.dbn != nil) {
        [[NSNotificationCenter defaultCenter]  removeObserver:self name:self.dbn object:nil];
    }
    self.dbn = dbn;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadImage:) name:self.dbn object:nil];
    
}

- (void)loadImage:(NSNotification *)notif {
    
    NSString *urlString = (NSString *)notif.object;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    
}

- (void)setupDelegate:(id<CardViewDelegate>)delegate {
    
    self.delegate = delegate;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapCard)];
    [self addGestureRecognizer:tapGesture];
    
}

- (void)didTapCard {
    [self.delegate cardTapped:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
