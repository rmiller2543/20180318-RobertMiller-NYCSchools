//
//  CardView.h
//  20180314-RM-NYCSchools
//
//  Created by Robert Miller on 3/18/18.
//  Copyright © 2018 RMiller. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CardViewDelegate<NSObject>
-(void)cardTapped:(UIView *)card;
@end

@interface CardView : UIView

@property(nonatomic,weak) IBOutlet UIImageView *imageView;
@property(nonatomic,weak) IBOutlet UILabel *nameLabel;
@property(nonatomic,assign) NSInteger index;

@property(nonatomic,weak) id<CardViewDelegate> delegate;

- (void)listenForDbn:(NSString *)dbn;
- (void)setupDelegate:(id<CardViewDelegate>)delegate;

@end
