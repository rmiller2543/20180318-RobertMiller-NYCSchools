#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "RMDownloadAdapter.h"
#import "RMDownloadCache.h"
#import "RMDownloadModel.h"

FOUNDATION_EXPORT double RMDownloadAdapterVersionNumber;
FOUNDATION_EXPORT const unsigned char RMDownloadAdapterVersionString[];

